package PaqueteChulo;

import java.util.Scanner;

public class TresEnRaya {

	// variable global: se usa en mas de una funcion, se declara fuera y estatica
	static String[][] tablero;
	static String turnos;

	public static void main(String[] args) {

		// siempre vamos a tener una funcion de inicializacion
		init();
		// un turno
		int arcaico=3;
		turnos = "卐";
		while (arcaico == 3) {
			mostrar(tablero);
			System.out.println("Turno de " + turnos +":");
			ponerFicha(turnos);
			turnos = cambiarTurnos(turnos);
			arcaico = comprobarVictoria(tablero);
		}
		mostrar(tablero);
	}

	private static void init() {
		// TODO Auto-generated method stub
		tablero = new String[3][3];
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				tablero[i][j]=" ";
			}
		}
	}
	
	private static void mostrar(String tablero[][]) {
		// TODO Auto-generated method stub
		for (int i = 0; i < tablero.length; i++) {
			for (int j = 0; j < tablero[0].length; j++) {
				System.out.print(tablero[i][j] +" ");
			}
			System.out.println();
		}
	}

	private static void ponerFicha(String turnos2) {
		Scanner tecla = new Scanner(System.in);
		// TODO Auto-generated method stub
		boolean seguir=true;
		while(seguir){
			int fila = tecla.nextInt();
			int columna = tecla.nextInt();
			if(tablero[fila][columna]!=" " ) {
				System.out.println("Error, vuelve a introducir:");
			} else {
				tablero[fila][columna]= turnos2;
				seguir=false;
			}
		}
		tecla.close();
	}

	private static String cambiarTurnos(String turnos2) {
		if(turnos2.equals("卐")) {
			return "✡";
		}else {
			return "卐";
		}
	}

	private static int comprobarVictoria(String tablero[][]) {
		//TODO
		int suma=1;;
		String ganador = " ";
		//combrobarFilas
		for (int i = 1; i < tablero.length; i++) {
			suma=1;
			for (int j = 1; j < tablero[0].length; j++) {
				if(tablero[i][j]==tablero[i-1][j]) {
					suma++;
				}
				if(suma==3) {
					ganador=tablero[i][j];
				}
			}
			if(suma==3) {
				break;
			}
		}
		//comprobarColumnas
		if(suma!=3) {
			for (int j = 1; j < tablero[0].length; j++) {
				suma=1;
				for (int i = 1; i < tablero.length; i++) {
					if(tablero[i][j]==tablero[i][j-1]) {
						suma++;
					}
					if(suma==3) {
						ganador=tablero[i][j];
					}
				}
				if(suma==3) {
					break;
				}
			}
		}
		//comprobarDiagonal
		if(suma!=3) {
				suma=1;
				for (int i = 1; i < tablero.length; i++) {
					if(tablero[i][i]==tablero[i-1][i-1]) {
						suma++;
					}
					if(suma==3) {
						ganador=tablero[i][i];
					}
				}
		}
		//comprobarSegundaDiagonal
		if(suma!=3) {
				suma=1;
				if(tablero[1][1]==tablero[0][2]) {
					suma++;
				}
				if(tablero[2][0]==tablero[1][1]) {
					suma++;
				}
				if(suma==3) {
					ganador=tablero[1][1];
				}
		}
		
		if (ganador.equals(" ")) {
			boolean empate=true;
			for (int i = 0; i < tablero.length; i++) {
				for (int j = 0; j < tablero[0].length; j++) {
					if(tablero[i][j].equals(" ")) {
						empate=false;
					}
				}
			}
			if(empate) {
				System.out.println("Habeis empatado.");
				return 2;
			}
			else {
				return 3;
			}
		}
		else if(ganador.equals("卐")) {
			System.out.println("Ha ganado 卐.");
			return 1;
		}
		else if(ganador.equals("✡")) {
			System.out.println("Ha ganado ✡.");
			return 0;
		}
		return 3;
	}

}