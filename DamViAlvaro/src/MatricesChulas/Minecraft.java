package MatricesChulas;

import java.util.Random;
import java.util.Scanner;

import Tetris.Board;
import Tetris.Window;

public class Minecraft {
	
	///tablero
	static int[][] tablero = new int[12][12]; // de 0 a 31 (-bordes = del 1 al 30)
	static int[][] tablerojuego = new int[12][12];
	//numero de minas
	static int minas = 20;
	
	//static Board b = new Board();
	//static Window w = new Window(b);
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tecla = new Scanner(System.in);
		//b.setActcolors(false);
		//activar modo sprites
		//b.setActsprites(true);
		//b.setActimgbackground(true);
		//creo el vector de sprites a mano
		//String[] sprites = {"0", "1", "2", "3","4","5","6","7","8","nz.png", "10"};
		//le paso el vector que acabo de crear al motor
		//b.setSprites(sprites);
		//le paso al motor un fondo
		//b.setImgbackground("ger.png");
		rellenarTablero();
		Utils.viewbordes(tablerojuego);
		//b.draw(tablerojuego);
		int fila = 0;
		int columna = 0;
		while (tablero[fila][columna]!=9) {
			System.out.println("Introduce dos numero entre 1 y "+ ((tablero[0].length)-2));
			fila = tecla.nextInt();
			columna = tecla.nextInt();
			actualizarTableroJuego(fila, columna);
			Utils.viewbordes(tablerojuego);
			//b.draw(tablerojuego);
		}
	}

	private static void rellenarTablero() {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				tablero[f][c]=0;
				tablerojuego[f][c]=0;
			}
		}
		Random r = new Random();
		int randx = r.nextInt((tablero.length)-1)+1; //del 1 al 30
		int randy = r.nextInt((tablero[0].length)-1)+1;		
		for(int i = 0; i <minas; i++) {
			while(tablero[randx][randy]==9) {
				randx = r.nextInt((tablero.length)-2)+1;
				randy = r.nextInt((tablero[0].length)-2)+1;	
			}
			tablero[randx][randy]=9;
		}
		for (int f = 1; f < tablero.length-1; f++) {
			for (int c = 1; c < tablero[0].length-1; c++) {
				if(tablero[f][c]==0) {
					int suma=0;
					for (int i = f-1; i <= f+1; i++) {
						for (int j = c-1; j <= c+1; j++) {
							if(tablero[i][j]==9) {
								suma++;
							}
						}
					}
					tablero[f][c]=suma;
				}
			}
		}
	}
	
	private static void actualizarTableroJuego(int x, int y) {
		tablerojuego[x][y]=tablero[x][y];
		if(tablero[x][y]!=9) {
			if(tablero[x][y]==0) {
				tablerojuego[x][y]=10;
			}
			System.out.println("Te has salvado");
		} else {
			System.out.println("Te has muerto");
		}
	}
	
	
}
