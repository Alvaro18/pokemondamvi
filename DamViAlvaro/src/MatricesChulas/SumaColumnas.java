package MatricesChulas;

import java.util.Scanner;

public class SumaColumnas {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner tecla = new Scanner(System.in);
		
		int casos = tecla.nextInt();
		
		for (int cas = 0; cas<casos; cas++) {

			int filas = tecla.nextInt();
			int columnas = tecla.nextInt();

			int[][] matriz = new int[filas][columnas];
			int[] sumaColumnas = new int[columnas];
			
			for(int f=0; f<matriz.length;f++){				
				for(int c=0; c<matriz.length;c++){
					matriz[f][c] = tecla.nextInt();
				}
			}
			
			for(int c=0; c<matriz.length;c++){
				for(int f=0; f<matriz.length;f++){				
					sumaColumnas[c] += matriz[f][c];
				}
			}

			
			for(int f=0; f<matriz.length;f++){
				for(int c=0; c<matriz[0].length;c++){
					System.out.print(matriz[f][c]+" ");
				}
				System.out.println();
			}
			
			for(int c=0; c<matriz[0].length;c++){
				System.out.print(sumaColumnas[c]+" ");
			}
			System.out.println();
		}
		
		tecla.close();
	}

}
