package MatricesChulas;

public class Utils {
	
	public static void view(int[][] tablero) {
		for (int f = 0; f < tablero.length; f++) {
			for (int c = 0; c < tablero[0].length; c++) {
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
	}
	
	public static void viewbordes(int[][] tablero) {
		for (int f = 1; f < tablero.length-1; f++) {
			for (int c = 1; c < tablero[0].length-1; c++) {
				System.out.print(tablero[f][c]+" ");
			}System.out.println();
		}
		System.out.println("-------------------------------");
	}

}
